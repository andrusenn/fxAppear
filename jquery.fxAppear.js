/*
 * jQuery fxAppear v1.0
 * used with animate.css https://daneden.github.io/animate.css/
 * 
 * @requires jQuery 1.9.0 or later
 * @requires animate.css
 *
 *
 * Copyright (c) 2017 Andrés Senn
 * andressenn.com
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html *
 */
(function ($) {
      $.fn.fxAppear = function () {
            return this.each(function () {
                  let delay = parseFloat($(this).data('delay')) || 0;
                  let duration = parseFloat($(this).data('duration')) || 1;
                  let efx = $(this).data('efx') || 'fadeInUp';
                  let iteration = $(this).data('iteration') || '1';
                  $(this).css({
                        // duration
                        'animation-duration': duration + 's',
                        '-webkit-animation-duration': duration + 's',
                        '-moz-animation-duration': duration + 's',
                        // delay
                        'animation-delay': delay + 's',
                        '-webkit-animation-delay': delay + 's',
                        '-moz-animation-delay': delay + 's',
                        // Iteration
                        'animation-iteration-count': iteration,
                        '-webkitanimation-iteration-count': iteration,
                        '-moz-animation-iteration-count': iteration,
                        'visibility': 'hidden'
                  });
                  $(window).on("scroll resize orientationchange", () => {
                        doAppear($(this));
                  });
                  doAppear($(this));

                  function doAppear(_this) {
                        if ($(_this).offset().top < $(window).scrollTop() +
                              $(window).innerHeight()) {
                              $(_this).addClass('animated ' + efx).css("visibility", "visible");
                        }
                  }
            });

      };
})(jQuery);